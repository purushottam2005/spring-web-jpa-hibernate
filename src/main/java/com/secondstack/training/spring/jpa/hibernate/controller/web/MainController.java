package com.secondstack.training.spring.jpa.hibernate.controller.web;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/19/13
 * Time: 1:31 PM
 * To change this template use File | Settings | File Templates.
 */

@Controller
@RequestMapping("/")
@SuppressWarnings("unchecked")
public class MainController {

    protected static Logger logger = Logger.getLogger("controller");

    @RequestMapping(method = RequestMethod.GET)
    public String home(ModelMap modelMap) {
        logger.debug("Received request to show home");

        modelMap.addAttribute("menuHomeClass", "active");

        return "home-tiles";
    }

}
