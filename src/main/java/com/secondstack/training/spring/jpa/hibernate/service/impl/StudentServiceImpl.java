package com.secondstack.training.spring.jpa.hibernate.service.impl;

import com.secondstack.training.spring.jpa.hibernate.domain.Student;
import com.secondstack.training.spring.jpa.hibernate.service.StudentService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/22/13
 * Time: 9:26 AM
 * To change this template use File | Settings | File Templates.
 */
@Service
@Transactional
public class StudentServiceImpl implements StudentService {

    @PersistenceContext
    protected EntityManager entityManager;

    @Override
    public void save(Student student) {
        entityManager.persist(student);
    }

    @Override
    public Student update(Integer id, Student student){
        Student oldStudent = findById(id);
        oldStudent.setAddress(student.getAddress());
        oldStudent.setAge(student.getAge());
        oldStudent.setFirstName(student.getFirstName());
        oldStudent.setLastName(student.getLastName());
        oldStudent.setSex(student.getSex());
        return entityManager.merge(oldStudent);
    }

    @Override
    public void delete(Student student){
        entityManager.remove(student);
    }

    @Override
    public void delete(Integer id){
        entityManager.remove(findById(id));
    }

    @Override
    public List<Student> findAll(){
        List<Student> results = entityManager.createQuery("SELECT s from Student s", Student.class).getResultList();
        return results;
    }
    @Override
    public Student findById(Integer id){
        return entityManager.find(Student.class, id);
    }
}
