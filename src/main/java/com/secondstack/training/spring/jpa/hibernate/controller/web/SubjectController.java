package com.secondstack.training.spring.jpa.hibernate.controller.web;

import com.secondstack.training.spring.jpa.hibernate.domain.Subject;
import com.secondstack.training.spring.jpa.hibernate.service.SubjectService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.sql.SQLException;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/19/13
 * Time: 9:08 PM
 * To change this template use File | Settings | File Templates.
 */
@Controller
@RequestMapping("subject")
public class SubjectController {

    protected static Logger logger = Logger.getLogger("controller");

    @Autowired
    private SubjectService subjectService;

    @RequestMapping(value = "/form", method = RequestMethod.GET)
    public String form(ModelMap modelMap) {
        logger.debug("Received request to get form subject");

        Subject subject = new Subject();
        modelMap.addAttribute("subject", subject);
        modelMap.addAttribute("subjectUrl", "/subject");
        modelMap.addAttribute("menuSubjectClass", "active");

        return "subject-form-tiles";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String create(@ModelAttribute("subject") Subject subject, ModelMap modelMap) throws SQLException {
        logger.debug("Received request to create subject");

        subjectService.save(subject);
        modelMap.addAttribute("subject", subject);
        modelMap.addAttribute("menuSubjectClass", "active");

        return "subject-data-tiles";
    }

    @RequestMapping(value = "form", params = "id", method = RequestMethod.GET)
    public String form(@RequestParam("id")Integer id, ModelMap modelMap) throws SQLException {
        logger.debug("Received request to get form subject");

        Subject subject = subjectService.findById(id);
        modelMap.addAttribute("subject", subject);
        modelMap.addAttribute("subjectUrl", "/subject?id=" + id);
        modelMap.addAttribute("menuSubjectClass", "active");

        return "subject-form-tiles";
    }

    @RequestMapping(params = "id", method = RequestMethod.POST)
    public String update(@RequestParam("id")Integer id, @ModelAttribute("subject") Subject subject, ModelMap modelMap) throws SQLException {
        logger.debug("Received request to update subject");

        subjectService.update(id, subject);
        modelMap.addAttribute("subject", subject);
        modelMap.addAttribute("menuSubjectClass", "active");

        return "subject-data-tiles";
    }

    @RequestMapping(method = RequestMethod.GET)
    public String findAll(ModelMap modelMap) throws SQLException{
        logger.debug("Received request to get list subject");

        List<Subject> subjectList = subjectService.findAll();
        modelMap.addAttribute("subjectList", subjectList);
        modelMap.addAttribute("menuSubjectClass", "active");

        return "subject-list-tiles";
    }

    @RequestMapping(params = {"id"}, method = RequestMethod.GET)
    public String findById(@RequestParam("id")Integer id, ModelMap modelMap) throws SQLException{
        logger.debug("Received request to get data subject");

        Subject subject = subjectService.findById(id);
        modelMap.addAttribute("subject", subject);
        modelMap.addAttribute("menuSubjectClass", "active");

        return "subject-data-tiles";
    }

    @RequestMapping(value = "delete", params = {"id"}, method = RequestMethod.GET)
    public String delete(@RequestParam("id")Integer id, ModelMap modelMap) throws SQLException{
        logger.debug("Received request to delete subject");

        subjectService.delete(id);

        return "redirect:/subject";
    }

}
