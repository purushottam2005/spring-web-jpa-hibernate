package com.secondstack.training.spring.jpa.hibernate.service.impl;

import com.secondstack.training.spring.jpa.hibernate.domain.Subject;
import com.secondstack.training.spring.jpa.hibernate.service.SubjectService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: LATIEF-NEW
 * Date: 4/22/13
 * Time: 9:26 AM
 * To change this template use File | Settings | File Templates.
 */
@Service
@Transactional
public class SubjectServiceImpl implements SubjectService {

    @PersistenceContext
    protected EntityManager entityManager;

    @Override
    public void save(Subject subject) {
        entityManager.persist(subject);
    }

    @Override
    public Subject update(Integer id, Subject subject){
        Subject oldSubject = findById(id);
        oldSubject.setCode(subject.getCode());
        oldSubject.setName(subject.getName());
        oldSubject.setSks(subject.getSks());
        return entityManager.merge(oldSubject);
    }

    @Override
    public void delete(Subject subject){
        entityManager.remove(subject);
    }

    @Override
    public void delete(Integer id){
        entityManager.remove(entityManager.find(Subject.class, id));
    }

    @Override
    public List<Subject> findAll(){
        List<Subject> results = entityManager.createQuery("SELECT o from Subject o", Subject.class).getResultList();
        return results;
    }
    @Override
    public Subject findById(Integer id){
        return entityManager.find(Subject.class, id);
    }
}
