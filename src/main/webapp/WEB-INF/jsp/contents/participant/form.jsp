<%--
  Created by IntelliJ IDEA.
  User: LATIEF-NEW
  Date: 4/19/13
  Time: 10:25 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<div class="alert" style="background-color: #ffffff;">

    <div class="alert alert-info">
        <h4>Insert Participant Data</h4>
    </div>

    <form:form modelAttribute="lectureList" method="POST" action="${participantUrl}" class="">

        <table class="table table-striped">
            <thead>
            <th>Choose</th>
            <th>Subject</th>
            <th>Teacher</th>
            <th>Semester</th>
            <th>Year</th>
            </thead>
            <tbody>

            <c:forEach items="${lectureList}" var="lecture" varStatus="i">
                <tr>
                    <td><input type="checkbox" name="lectureList[${i.index}].id" value="${lecture.id}"/></td>
                    <td>${lecture.subject.name}</td>
                    <td>${lecture.teacher.firstName}</td>
                    <td>${lecture.semester}</td>
                    <td>${lecture.year}</td>
                </tr>
            </c:forEach>

            </tbody>
        </table>

        <input class="btn" type="reset" value="Reset"/>
        <input class="btn btn-primary" type="submit" value="Submit"/>
    </form:form>
</div>
