<%--
  Created by IntelliJ IDEA.
  User: LATIEF-NEW
  Date: 4/19/13
  Time: 10:25 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="alert" style="background-color: #ffffff;">

    <div class="alert alert-info">
        <h4>This Lecture Data</h4>
    </div>
    <table>
        <tr>
            <td>Subject :</td>
            <td>${lecture.subject.name}</td>
        </tr>
        <tr>
            <td>Teacher :</td>
            <td>${lecture.teacher.firstName}</td>
        </tr>
        <tr>
            <td>Semester :</td>
            <td>${lecture.semester}</td>
        </tr>
        <tr>
            <td>Year :</td>
            <td>${lecture.year}</td>
        </tr>
    </table>
    <br>
    <a href="<c:url value='/lecture'/>"><button class="btn">Back</button></a>
</div>
