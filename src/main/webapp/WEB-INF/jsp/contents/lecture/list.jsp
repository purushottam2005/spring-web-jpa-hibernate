<%--
  Created by IntelliJ IDEA.
  User: LATIEF-NEW
  Date: 4/19/13
  Time: 10:25 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="alert" style="background-color: #ffffff;">

    <div class="alert alert-info">
        <h4>
            Lecture List
            <a href="<c:url value='/lecture/form'/>">
                <button class="btn btn-small">
                    <i class="icon-plus"></i>
                </button>
            </a>
        </h4>
    </div>

    <table class="table table-striped">

        <thead>
        <td>Id</td>
        <td>Subject</td>
        <td>Teacher</td>
        <td>Semester</td>
        <td>Year</td>
        <td>Action</td>
        </thead>

        <tbody>

        <c:forEach items="${lectureList}" var="lecture">
            <tr>
                <td><a href="<c:url value='/lecture?id=${lecture.id}'/>">${lecture.id}</a></td>
                <td>${lecture.subject.name}</td>
                <td>${lecture.teacher.firstName}</td>
                <td>${lecture.semester}</td>
                <td>${lecture.year}</td>
                <td>
                    <a href="<c:url value='/lecture/form?id=${lecture.id}'/>">
                        <button class="btn btn-primary">
                            <i class="icon-white icon-edit"></i>
                        </button>
                    </a>
                    <a href="<c:url value='/lecture/delete?id=${lecture.id}'/>">
                        <button class="btn btn-primary btn-danger">
                            <i class="icon-white icon-remove"></i>
                        </button>
                    </a>
                </td>
            </tr>
        </c:forEach>

        </tbody>

    </table>

</div>
