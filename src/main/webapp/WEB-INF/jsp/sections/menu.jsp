<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<div class="navbar">
    <div class="navbar-inner">
        <div class="container">
            <ul class="nav">
                <li class="${menuHomeClass}"><a href="<c:url value='/'/>">Home</a></li>
                <li class="${menuStudentClass }"><a href="<c:url value='/student'/>">Student</a></li>
                <li class="${menuTeacherClass }"><a href="<c:url value='/teacher'/>">Teacher</a></li>
                <li class="${menuSubjectClass }"><a href="<c:url value='/subject'/>">Subject</a></li>
                <li class="${menuLectureClass }"><a href="<c:url value='/lecture'/>">Lecture</a></li>
            </ul>
        </div>
    </div>
</div>